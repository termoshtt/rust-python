ARG PYTHON_VERSION
FROM python:${PYTHON_VERSION}-slim
ARG RUST_VERSION

RUN apt-get update \
 && apt-get install -y curl gcc gfortran cmake make \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

# From official setting in https://github.com/rust-lang/docker-rust
ENV RUSTUP_HOME=/usr/local/rustup
ENV CARGO_HOME=/usr/local/cargo
ENV PATH=/usr/local/cargo/bin:$PATH

# Setup Rust
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --no-modify-path --default-toolchain ${RUST_VERSION}
# this may concern security issue for production use, but this container is designed for development use.
RUN chmod -R a+w $RUSTUP_HOME $CARGO_HOME

RUN python -m pip install maturin
