PYTHON_VERSIONS := 3.6 3.7 3.8
RUST_VERSIONS   := 1.43.0

CI_REGISTRY_IMAGE  ?= registry.gitlab.com/termoshtt/rust-python
CI_COMMIT_REF_SLUG ?= manual

define rust-python
python$(1)-rust$(2):
	docker build \
		--build-arg="PYTHON_VERSION=$(1)" \
		--build-arg="RUST_VERSION=$(2)"   \
		-t $(CI_REGISTRY_IMAGE)/$$@:$(CI_COMMIT_REF_SLUG) .
	docker push $(CI_REGISTRY_IMAGE)/$$@:$(CI_COMMIT_REF_SLUG)
endef

all: $(foreach PYTHON_VERSION,$(PYTHON_VERSIONS),    \
       $(foreach RUST_VERSION,$(RUST_VERSIONS),      \
         python$(PYTHON_VERSION)-rust$(RUST_VERSION) \
       )                                             \
     )

$(foreach PYTHON_VERSION,$(PYTHON_VERSIONS),                      \
  $(foreach RUST_VERSION,$(RUST_VERSIONS),                        \
    $(eval $(call rust-python,$(PYTHON_VERSION),$(RUST_VERSION))) \
  )                                                               \
)
